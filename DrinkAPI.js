$(document).ready(function(){

    onPageLoading();

    function onPageLoading(){
        getDrinkAPI();
    }

    function getDrinkAPI(){
        $.ajax({
            url: "http://localhost:8080/api/drinks",
            type: "GET",
            success: function(response){
                console.log(response);
                loadDataToSelectDrink(response);
            },
            error: function(err){
                console.log(err.status);
            }
        })
    }

    function loadDataToSelectDrink(paramDrink) {
        
        for (let index = 0; index < paramDrink.length; index++) {
            $("#select-drink").append($("<option></option>", {
                text: paramDrink[index].tenNuocUong,
                value: paramDrink[index].maNuocUong
            }))
            
        }
    }   

})