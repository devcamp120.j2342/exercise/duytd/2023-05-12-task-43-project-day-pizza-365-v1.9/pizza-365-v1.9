$(document).ready(function(){
    //REGION 1
    const gPIZZA_HAISAN = "HaiSan";
    const gPIZZA_HAWAI = "HaWai";
    const gPIZZA_CHICKEN = "Chicken";
    //REGION 2
    //gán sự kiện nút chọn loại pizza hải sản
    $("#btn-hai-san").on('click', function(){
        onBtnHaiSanClick();
    })
    //gán sự kiện nút chon loại pizza hawai
    $("#btn-hawai").on("click", function(){
        onBtnHawaiClick();
    })
    //gán sự kiện nút chọn loại pizza chicken bacon
    $("#btn-chicken-bacon").on('click', function(){
        onBtnChickenBaconClick();
    })
    //REGION 3
    //hàm thực hiện sự kiện nút btn chọn hải sản được click
    function onBtnHaiSanClick(){
        //thay đổi màu khi chọn nút
        changeTypePizzaBtnColor(gPIZZA_HAISAN);
        console.log("pizza được chọn: " + gPIZZA_HAISAN);
    }
    //hàm thực hiện sự kiện nút btn chon hawai được click
    function onBtnHawaiClick(){
        //thay đổi màu khi chọn nút
        changeTypePizzaBtnColor(gPIZZA_HAWAI);
        console.log("pizza được chọn: " + gPIZZA_HAWAI);
    }
    //hàm thực hiện sự kiện nút btn chọn chicken bacon được click
    function onBtnChickenBaconClick(){
        //thay đổi màuk khi chọn nút
        changeTypePizzaBtnColor(gPIZZA_CHICKEN);
        console.log("pizza được chọn: " + gPIZZA_CHICKEN);
    }
    //REGION 4
    //hàm thực hiển thây đổi color khi nút được chọn kiểu pizza
    function changeTypePizzaBtnColor(paramTypePizza){
        var vBtnHaiSan = $("#btn-hai-san"); //truy xuất nút chọn tại kiểu pizza hải sản
        var vBtnHawai = $("#btn-hawai"); //truy xuất nút chọn tại kiểu pizza hawai
        var vBtnChicken = $("#btn-chicken-bacon"); //truy xuất nút chọn tại kiêu pizza chocken

        if(paramTypePizza == gPIZZA_HAISAN){
            vBtnHaiSan.css("background-color", "green");
            vBtnHaiSan.data("is-selected-pizza", "Y");
            vBtnHawai.css("background-color", "orange");
            vBtnHawai.data("is-selected-pizza", "N");
            vBtnChicken.css("background-color", "orange");
            vBtnChicken.data("is-selected-pizza", "N")
        }else if(paramTypePizza == gPIZZA_HAWAI){
            vBtnHaiSan.css("background-color", "orange");
            vBtnHaiSan.data("is-selected-pizza", "N");
            vBtnHawai.css("background-color", "green");
            vBtnHawai.data("is-selected-pizza", "Y");
            vBtnChicken.css("background-color", "orange");
            vBtnChicken.data("is-selected-pizza", "N")
        }   
        else if(paramTypePizza == gPIZZA_CHICKEN){
            vBtnHaiSan.css("background-color", "orange");
            vBtnHaiSan.data("is-selected-pizza", "N");
            vBtnHawai.css("background-color", "orange");
            vBtnHawai.data("is-selected-pizza", "N");
            vBtnChicken.css("background-color", "green");
            vBtnChicken.data("is-selected-pizza", "Y");
        }
    }

})