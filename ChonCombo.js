$(document).ready(function(){
    //REGION 1 Global variables - vùng khai báo biến, hằng số, tham số TOÀN CỤC
    const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail";
    const gCOMBO_SMALL = "small";
    const gCOMBO_MEDIUM = "medium";
    const gCOMBO_LARGE = "large"; 
    var gSelected_Small = getDataComboSelected("S", 20, 2, "200g", 2, 150000);
    var gSelected_Medium = getDataComboSelected("M", 25, 4, "300g", 3, 200000);
    var gSelected_Large = getDataComboSelected("L", 30, 8, "500g", 4, 250000);
    //REGION 2 Vùng gán - thực thi hàm sử lý sự kiện cho các elements
  
    //gán sự kiện nút btn small
    $("#btn-small").on('click', function(){
        onBtnSmallClick();
    });
    //gán sự kiện nút medium
    $("#btn-medium").on('click', function(){
        onBtnMediumClick();
    });
    //gán sự kiện nút lager
    $("#btn-large").on('click', function(){
        onBtnLargeClick();
    });
    //gán sự kiện nút gửi
    $("#btn-send").on('click', function(){
        onBtnSendOrderClick();
    });
    //gán sự kiện nút tạo đơn trên modal
    $("#btn-create-order").click(function(){
        onBtnTaoDonClick();
    });
    //REGION 3 Event headlers - vùng khai báo các hàm sử lý sự kiện

    //hàm sử lý sự kiện nút btn small được click
    function onBtnSmallClick(){
        //hàm đổi màu khi chọn nút btn small
        changeComboBtnColor(gCOMBO_SMALL);
        //gọi phương thức hiển thị thông tin
        gSelected_Small.displayInConsoleLog();
    };
    //hàm sử lý sự kiện nút btn medium được click
    function onBtnMediumClick(){
        //hàm đổi màu khi chọn nút btn small
        changeComboBtnColor(gCOMBO_MEDIUM);
        //gọi phương thức hiển thị thông tin
        gSelected_Medium.displayInConsoleLog();
    };
    //hàm sử lý sự kiện nút btn lager được click
    function onBtnLargeClick(){
        //hàm đổi màu khi chọn nút btn small
        changeComboBtnColor(gCOMBO_LARGE);
        //gọi phương thức hiển thị thông tin
        gSelected_Large.displayInConsoleLog();
    }
    //hàm sử lý sự kiện nút gửi được click
     function onBtnSendOrderClick(){
        //b1 chuẩn bị dữ liệu, thu thập dữ liệu
        var vOrder = getDataOrder()
        //b2 kiểm tra dữ liệu
        var vIsCheckOrder = validateOrder(vOrder);
        if(vIsCheckOrder){

            //b3 hiển thị thông tin
            showDataOrderToModal(vOrder);
            $("#modal-order").modal('show');
        }
    }
    //hàm sử lý sự kiện nút tạo đơn được click
    function onBtnTaoDonClick(){
        // b1 chuẩn bị dữ liệu
        var vOrder = getDataOrder()
        // b2 kiểm tra dữ liệu
        // b3 gọi api
        callApiCreateOrder(vOrder);
        // b4 xử lý hiển thị
        $("#modal-order").modal('hide');
        $("#modal-tao-don").modal('show');
        
    }
    //REGION 4 Commens functions - vùng khai báo hàm dùng chung cho toàn bộ chương trình
    //hàm đổi màu nút khi chọn combo
    function changeComboBtnColor(paramCombo){
        var vBtnSmall = $("#btn-small"); //truy vấn nút chọn btn small
        var vBtnMedium = $("#btn-medium"); //truy vấn nút chọn btn medium
        var vBtnLarge = $("#btn-large"); //truy vấn nút chọn btn large

        if(paramCombo == gCOMBO_SMALL){
            vBtnSmall.css("background-color", "green");
            vBtnSmall.data("is-selected-menu", "Y");
            vBtnMedium.css("background-color", "orange");
            vBtnMedium.data("is-selected-menu", "N");
            vBtnLarge.css("background-color", "orange");
            vBtnLarge.data("is-selected-menu", "N");
        }
        else if(paramCombo == gCOMBO_MEDIUM){
            vBtnSmall.css("background-color", "orange");
            vBtnSmall.data("is-selected-menu", "N");
            vBtnMedium.css("background-color", "green");
            vBtnMedium.data("is-selected-menu", "Y");
            vBtnLarge.css("background-color", "orange");
            vBtnLarge.data("is-selected-menu", "N");
        }
        else if(paramCombo == gCOMBO_LARGE){
            vBtnSmall.css("background-color", "orange");
            vBtnSmall.data("is-selected-menu", "N");
            vBtnMedium.css("background-color", "orange");
            vBtnMedium.data("is-selected-menu", "N");
            vBtnLarge.css("background-color", "green");
            vBtnLarge.data("is-selected-menu", "Y");
        }
    }
    //hàm lấy dữ liệu menu được chọn và trả lại đối tượng được tham số hóa
    function getDataComboSelected(
        paramMenuName,
        paramDuongKinh,
        paramSuon,
        paramSalad,
        paramDrink,
        paramPrice
    ){
        var vSelectComboMenu = {
            menuName: paramMenuName,
            duongKinh: paramDuongKinh,
            suon: paramSuon,
            salad: paramSalad,
            drink: paramDrink,
            price: paramPrice,
            displayInConsoleLog(){
                console.log("tên menu: " + this.menuName);
                console.log("đường kính: " + this.duongKinh);
                console.log("suon: " + this.suon);
                console.log("salad: " + this.salad);
                console.log("nước: " + this.drink);
                console.log("thanh tiền: " + this.price);
            }
        }
        return vSelectComboMenu;
    }
    //hàm thu thập (đọc) dữ liệu khách hàng
    function getDataOrder(){
        //thu thập (đọc) menu combo
        var vBtnSmall = $("#btn-small");
        var vMenuSmallSelected = vBtnSmall.data("is-selected-menu");
        var vBtnMedium = $("#btn-medium");
        var vMenuMediumSelected = vBtnMedium.data("is-selected-menu");
        var vBtnLarge = $("#btn-large");
        var vMenuLargeSelected = vBtnLarge.data("is-selected-menu");

        var vSelectedMenuStructure = getDataComboSelected("", 0, 0, "", 0, 0);

        if(vMenuSmallSelected === "Y"){
            vSelectedMenuStructure = gSelected_Small;
        }else if(vMenuMediumSelected === "Y"){
            vSelectedMenuStructure = gSelected_Medium;
        }else if(vMenuLargeSelected === "Y"){
            vSelectedMenuStructure = gSelected_Large;
        }
        //thu thập (đọc) kiêu pizza
        var vBtnHaiSan = $("#btn-hai-san");
        var vPizzaHaiSanSelected = vBtnHaiSan.data("is-selected-pizza");
        var vBtnHawai = $("#btn-hawai");
        var vPizzaHawaiSelected = vBtnHawai.data("is-selected-pizza");
        var vBtnChicken = $("#btn-chicken-bacon");
        var vPizzaChickenSelect = vBtnChicken.data("is-selected-pizza");
        
        var vSelectedPizzaStructure = "";

        if(vPizzaHaiSanSelected == "Y"){
            vSelectedPizzaStructure = "haiSan";
        }else if(vPizzaHawaiSelected == "Y"){
            vSelectedPizzaStructure = "haWai";
        }else if(vPizzaChickenSelect == "Y"){
            vSelectedPizzaStructure = "chicken";
        }
        //thu thâp (đọc) dưc liệu drink
        var vValueSelectDrink = $("#select-drink").val()
        //thu thập dữ liệu từ form
        var vValueHoTen = $("#inp-ho-ten").val().trim();
        var vValueEmail = $("#inp-email").val().trim();
        var vValuePhone = $("#inp-phone").val().trim();
        var vValueDiaChi = $("#inp-dia-chi").val().trim();
        var vValueVoucher = $("#inp-voucher").val().trim();
        var vValueMessage = $("#inp-message").val().trim();

        // khai báo biến chứa phần trăm giảm giá
        var vPercent = 0;
        //tính phần trăm giảm giá
        var vVoucherObj = getVoucherById(vValueVoucher);
        if(vValueVoucher !== "" && vVoucherObj !== null){
            vPercent = vVoucherObj.phanTramGiamGia;
        }
        //tạo đối tượng để lưu dữ liệu
        var vOrderObj = {
            menuCombo: vSelectedMenuStructure,
            typePizza: vSelectedPizzaStructure,
            drink: vValueSelectDrink,
            hoTen: vValueHoTen,
            email: vValueEmail,
            soDienThoai: vValuePhone,
            diaChi: vValueDiaChi,
            voucher: vValueVoucher,
            loiNhan: vValueMessage,
            phanTramGiamGiaa: vPercent,
            priceAnnualVND: function(){
                var vTotal = this.menuCombo.price * (1 - this.phanTramGiamGiaa / 100);
                return vTotal;
            }
        }
        return vOrderObj;
    }
    //hàm kiểm tra dữ liệu order
    function validateOrder(paramOrder){
        if(paramOrder.menuCombo.menuName == ""){
            alert("Hãy chọn Menu");
            return false;
        };
        if(paramOrder.typePizza == ""){
            alert("Bạn chưa chọn loại pizza");
            return false;
        }
        if(paramOrder.drink == "0"){
            alert("Hãy chọn nước uống");
            return false;
        };
        if(paramOrder.hoTen == ""){
            alert("Họ tên chưa được nhập");
            return false;
        };
        if(paramOrder.email == ""){
            alert("Email chưa được nhập");
            return false;
        };
        if(paramOrder.soDienThoai == ""){
            alert("Phone chưa được nhập");
            return false;
        };
        if(paramOrder.diaChi == ""){
            alert("Bạn cần nhập địa chỉ");
            $("#inp-dia-chi").focus();
            return false;
        };
        return true;
    }
    // //hàm tìm voucher qua id
    function getVoucherById(paramVoucherId){
        if(paramVoucherId === ""){
            return;
        }
        var vVoucherObj = {};
        $.ajax({
            url: gBASE_URL + "/" + paramVoucherId,
            type: "GET",
            async: false,
            success: function(response){
                vVoucherObj = response;
                console.log(vVoucherObj);
            },
            error: function(err){
                console.log(err.status);
            }
        });
        return vVoucherObj;
    }
    //hàm hiển thị thông tin order lên modal
    function showDataOrderToModal(paramOrder){
        var vModalHoTen = $("#modal-inp-ho-ten").val(paramOrder.hoTen);
        var vModalPhone = $("#modal-inp-phone").val(paramOrder.soDienThoai);
        var vModalDiaChi = $("#modal-inp-dia-chi").val(paramOrder.diaChi);
        var vModalMessage = $("#modal-inp-message").val(paramOrder.loiNhan);
        var vModalVoucher = $("#modal-inp-voucher").val(paramOrder.voucher);
        var vThongTin = "Xác nhận: " + paramOrder.hoTen + ", " + paramOrder.soDienThoai + ", " + paramOrder.diaChi + "\n" +
                        "Menu: " + paramOrder.menuCombo.menuName + ", sườn nướng " + paramOrder.menuCombo.suon + ", nước " + paramOrder.menuCombo.drink + "\n" +
                        "Loại pizza: " + paramOrder.typePizza + ", Giá: " + paramOrder.menuCombo.price + "vnd, " + "Mã giảm giá: " + paramOrder.voucher + "\n" +
                        "Phải thanh toán: " + paramOrder.priceAnnualVND() + "vnd" + ", Phần trăm giảm giá " + paramOrder.phanTramGiamGiaa + "%"
        var vModalDetail = $("#modal-textarea").val(vThongTin);
    }
    //hàm thu thập dữ liệu
    function getDataOrderCode(paramOrderCode){
        paramOrderCode.maDonHang = $("#inp-orderCode").val();
    }
    //hàm gọi api để tạo thêm một order mới
    function callApiCreateOrder(paramOrder){
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrder),
            success: function(response){
                showOrderCodeToModal(response.orderCode);
            },
            error: function(err){
                console.log()
            }
        });
    }
    //hàm hiển thị orderCode lên modal
    function showOrderCodeToModal(paramRes){
        $("#inp-orderCode").val(paramRes);
    }

});