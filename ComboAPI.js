$(document).ready(function(){

    var objects = [];

    onPageLoading();

    function onPageLoading() {
        callApigetComboMenu();
    }

    //gàm gọi api để lấy dữ liệu comboMenu
    function callApigetComboMenu() {
        $.ajax({
            url: "http://localhost:8080/combomenu",
            type: "GET",
            success: function(response){
                console.log(response);
                displayComboMenu(response);
            },
            error: function(err){
                console.log(err.status);
            }
        })
    }

    function displayComboMenu(paramData){
        $("#h3-s").html(paramData[0].size);
        $("#span-dk-s").html(paramData[0].duongKinh);
        $("#span-sn-s").html(paramData[0].suon);
        $("#span-sl-s").html(paramData[0].salad);
        $("#span-nn-s").html(paramData[0].soLuongNuocNgot);
        $("#h2-gia-s").html(paramData[0].donGia);

        $("#h3-m").html(paramData[1].size);
        $("#span-dk-m").html(paramData[1].duongKinh);
        $("#span-sn-m").html(paramData[1].suon);
        $("#span-sl-m").html(paramData[1].salad);
        $("#span-nn-m").html(paramData[1].soLuongNuocNgot);
        $("#h2-gia-m").html(paramData[1].donGia);

        $("#h3-l").html(paramData[2].size);
        $("#span-dk-l").html(paramData[2].duongKinh);
        $("#span-sn-l").html(paramData[2].suon);
        $("#span-sl-l").html(paramData[2].salad);
        $("#span-nn-l").html(paramData[2].soLuongNuocNgot);
        $("#h2-gia-l").html(paramData[2].donGia);
    }
})